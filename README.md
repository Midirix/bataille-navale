# MANUEL UTILISATEUR

## Introduction
Bienvenue dans le jeu de Bataille Navale. Ce manuel décrit le déroulement d'une partie ainsi que les actions poouvant être menées par les joueurs.

## Objectif du Jeu
Le but du jeu est de couler tous les bateaux de votre adversaire avant qu'il ne coule les vôtres. Chaque joueur place ses bateaux sur une grille et essaie de deviner les positions des bateaux adverses en tirant sur des cases spécifiques.

## Configuration du Jeu
Chaque joueur doit entrer son nom au début de la partie.
Un plateau de jeu de 10x10 cases est créé pour chaque joueur.
Chaque joueur dispose de plusieurs types de bateaux de différentes tailles à placer sur le plateau.

## Déroulement du Jeu
### Placement des bateaux
Les joueurs placent leurs bateaux (à l'abris des regards de l'adversaire) sur son plateau en indiquant tout d'abord le type de bateau souhaité. Attention, il faut orthographié correctement le nom du bateau comme affiché dans la liste des bateaux disponibles à l'écran : 
    - PA pour Porte-Avion
    - C pour Cuirassé
    - CT pour Contre-Torpilleur
    - P pour Patrouilleur

Le joueur doit ensuite renseigner les coordonnées de début et de fin pour chaque bateau. Il doit entrer un chiffre allant de 0 à 9. Attention à bien respecter la taille indiquée. Il faut aussi placer son bateau sur une seule ligne ou une seule colonne, un seul des 2 parametre doit varier entre la case de debut et de fin. Exemple : mon bateau a une taille de 4, je peux renseigner (ligne=3, colonne=2) pour la case de début et (ligne=6, colonne=2). Dans cet exemple, le bateau est placé à la verticale.
### Tour de Jeu
Les joueurs tirent à tour de rôle sur le plateau de l'adversaire pour tenter de toucher et couler ses bateaux. Il faut indiquer une ligne et une colonne pour tirer sur le plateau adverse. 
Vous rejouez si:
    - Vous tirez sur une case invalide
    - Vous tirez déjà tirée par vous aus tours précédents
    - Vous touchez un bateau
    - Vous coulez un bateau

## Conditions de Victoire
La partie continue jusqu'à ce qu'un des joueurs n'ait plus de bateaux sur son plateau. Le joueur ayant coulé tous les bateaux de l'adversaire est déclaré vainqueur. Le jeu vous proposera ensuite de rejouer. Les scores s'additionnent à chaque partie jusqu'à arrêt total du jeu.

## Commandes Utiles
Placer un bateau : Choisissez le type de bateau, puis entrez les coordonnées de début et de fin.
Tirer sur une case : Entrez les coordonnées de la case que vous souhaitez viser.

## Conclusion
Amusez-vous bien en jouant à la Bataille Navale ! Soyez stratégique dans le placement de vos bateaux et de vos tirs pour remporter la victoire.


