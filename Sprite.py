import pygame
import pygame_menu
import pygame_menu.menu
import pygame_menu.themes
from menu import *
from enum import Enum

vec = pygame.math.Vector2

class Boat():
    def __init__(self, sprite, orientation, size, base_pos, base_rot):
        self.position = vec(0, 0)
        self.position_plateau = vec(0, 0)
        self.position_plateau_base = base_pos
        self.rotation_base = base_rot
        self.origin = vec(0, 0)
        self.center = vec(0, 0)
        self.internal_scale = 0.45
        self.scale = 1.0
        if orientation == 0:
            self.sprite0 = sprite
        else:
            self.sprite0 = pygame.transform.rotozoom(sprite, 90, 1.0)
         
        self.sprite0.convert()
        self.sprite0.convert_alpha()
        
        self.sprite = self.sprite0
        self.rect = self.sprite.get_rect()
        self.width = self.sprite.get_width()
        self.rotation = 0;
        self.size = size
        self.moving = False
        self.initialized = False
        
    #Draw boat on screen    
    def blit_sprite(self, surface):
        surface.blit(self.sprite, self.position)
        
    #Set the position using pixel coordinates    
    def set_position_absolute(self, pos, jeu):
        self.set_scale(jeu.window_scale*jeu.plateau_scale)
        self.position = pos
        self.rect.update(pos.x, pos.y, self.rect.width, self.rect.height)
        self.origin = self.position + vec(self.width/2, self.width/2) 

    #Set the position using plateau coordinates        
    def set_position_game(self, pos, plateau, jeu): 
        self.set_position_absolute(jeu.plateau_to_absolute(plateau, pos, vec(self.width/2, self.width/2)), jeu)
    
    #Get the boat origin
    def get_origin(self):
        self.origin = self.position + vec(self.width/2, self.width/2)
        return self.origin
    
    def update_plateau_position(self, jeu):
        self.set_scale(jeu.window_scale*jeu.plateau_scale)
        self.position_plateau = jeu.absolute_to_plateau(0, self.position, vec(self.width/2, self.width/2))

    #Snap the boat to the plateau grid
    def snap_to_grid(self, jeu):
        if self.moving == False:
            return
        if jeu.plateau_A_init_done == False:
            snapped_position = self.snap_position(jeu)
            print("Snapped pos. : ",snapped_position)
            if jeu.is_boat_position_valid(self, snapped_position):
                print("Should set to True")
                self.position_plateau = snapped_position
                self.position = jeu.plateau_to_absolute(0, self.position_plateau, vec(self.width/2, self.width/2))
                self.rect.update(self.position.x, self.position.y, self.rect.width, self.rect.height)
                print("Before",self.initialized)
                self.initialized = True
                print("After",self.initialized)
                jeu.place_boat_on_plateau(self)
            else:
                self.position_plateau = self.position_plateau_base
                self.set_rotation(self.rotation_base)
                self.position = jeu.plateau_to_absolute(0, self.position_plateau, vec(self.width/2, self.width/2))
                self.rect.update(self.position.x, self.position.y, self.rect.width, self.rect.height)
                self.initialized = False

    #Snap the position to the grid            
    def snap_position(self, jeu):
        min_x = 0
        min_y = 0
        if self.rotation%180 == 0:
            max_x = 9
            max_y = 9-self.size+1
        else:
            max_y = 9
            max_x = 9-self.size+1
        snapped_position = vec(round(self.position_plateau.x), round(self.position_plateau.y))
        snapped_position.x = min(max(snapped_position.x, min_x), max_x)
        snapped_position.y = min(max(snapped_position.y, min_y), max_y)
        return snapped_position
    
    ###
    def is_position_valid(self, snapped_position):
        return True
    
    #Set the boat rotation
    def set_rotation(self, rot):    
        self.rotation = rot
        self.sprite = pygame.transform.rotozoom(self.sprite0, self.rotation, self.scale)
        self.rect = self.sprite.get_rect()
        self.rect.update(self.position.x, self.position.y, self.rect.width, self.rect.height)

    #Rotate by 90 degrees    
    def rotate(self):
        self.set_rotation(self.rotation+90)
    
    #Update the boat scale
    def update_scale(self, jeu):
        self.set_scale(jeu.window_scale*jeu.plateau_scale)
    
    #Set the plateau scale
    def set_scale(self, scale):
        self.scale = scale*self.internal_scale
        self.sprite = pygame.transform.rotozoom(self.sprite0, self.rotation, self.scale)
        if self.rotation%180 == 0:
            self.width = self.sprite.get_width()
        else:
            self.width = self.sprite.get_height()
        self.rect = self.sprite.get_rect()
        self.rect.update(self.position.x, self.position.y, self.rect.width, self.rect.height)