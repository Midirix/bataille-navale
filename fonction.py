from enum import Enum
import os
class case(Enum):
    VIDE= 0
    BATEAU = 1
    VIDE_TOUCHE = 2
    BATEAU_TOUCHE = 3

class tir(Enum):
    DEJA_TIRE= 0
    MANQUE = 1
    TOUCHE = 2
    COULE = 3

class tour(Enum):
    JOUEUR_1 = 0
    JOUEUR_2 = 1

class erreur_placer_bateau(Enum):
    OK = 0
    CASE_EN_DEHORS_PLATEAU = 1
    BATEAU_DIAGONAL = 2
    MAUVAISE_TAILLE_BATEAU = 3
    BATEAU_EN_DEHORS_PLATEAU = 1
    BATEAU_SUR_AUTRE_BATEAU = 4


joueur1 = {}#dict('name', 'plateau', 'bateaux','score')
joueur2 = {}#dict('name', 'plateau', 'bateaux','score')
joueur_actuel = ""
#tailles_bateaux={'PA':5,'C':4,'CT':3,'P':2}
#nb_bateaux={'PA':0,'C':0,'CT':2,'P':1}
def init_plateau(cases_h,cases_v):
    plateau=[[]]*cases_v
    for i in range(0,cases_v):
        plateau[i]=[case.VIDE]*cases_h
    return plateau

def init_dic_bateaux():
    dic_bateaux = {
        'PA' : { #Porte-Avion
            'Nombre' : 0,
            'Nombre_sur_plateau' : 0,
            'Taille' : 5,
            'Cases' : [[],[]]
        },
        'C' : { #Cuirassé
            'Nombre' : 0,
            'Nombre_sur_plateau' : 0,
            'Taille' : 4,
            'Cases' : [[]]
        },
        'CT' : { #Contre-torpilleur
            'Nombre' : 0,
            'Nombre_sur_plateau' : 0,
            'Taille' : 3,
            'Cases' : [[],[]]
        },
        'P' : { #Patrouilleur
            'Nombre' : 1,
            'Nombre_sur_plateau' : 0,
            'Taille' : 2,
            'Cases' : [[]]
        }
    }
    return dic_bateaux


#10 X10    1 PA 5 CASES             1 C 4 CASES             2   3CASES           1 2CASES    LISTE AVEC LES BATEAUX
def init_joueurs():
    joueur1['name'] = input("Nom joueur 1 : >")
    joueur1['score'] = 0
    joueur2['name'] = input("Nom joueur 2 : >")
    joueur2['score'] = 0

def init_partie():
    joueur1['plateau'] = init_plateau(10,10)
    joueur1['bateaux'] = init_dic_bateaux()
    joueur2['plateau'] = init_plateau(10,10)
    joueur2['bateaux'] = init_dic_bateaux()


def trouver_case_bateau(bateaux, ligne, colonne):
    for type_bat,valeurs_type_bat in bateaux.items(): #type_bat est un dictionnaire faisant reference à un type de bateaux (PA, C,...)
        #print("type_bat : ", type_bat, "\n\tvaleurs_type_bat : ", valeurs_type_bat)
        for numero_bateau in range(valeurs_type_bat['Nombre_sur_plateau']): #bat est un bateau, c'est une liste de cases
        #    print("numero_bateau :", numero_bateau)
            for numero_case in range(len(valeurs_type_bat['Cases'][numero_bateau])):
        #        print("(valeurs_type_bat['Cases'][numero_bateau][numero_case][1]", (valeurs_type_bat['Cases'][numero_bateau][numero_case][1]))
                if (valeurs_type_bat['Cases'][numero_bateau][numero_case][1] == ligne and valeurs_type_bat['Cases'][numero_bateau][numero_case][2] == colonne):
        #            print("jesuis dans le if")
        #            print("type_bat, numero_bateau, numero_case : ",type_bat, numero_bateau, numero_case)
                    return (type_bat, numero_bateau, numero_case)

def bateau_coulé(bateau): 
    if len(bateau) == 0:
        return 1
    return 0

def tirer(joueur,ligne,colonne) :
    plateau = joueur['plateau']
    dic_bateaux = joueur['bateaux']
    if(plateau[ligne][colonne] == case.VIDE_TOUCHE or plateau[ligne][colonne] == case.BATEAU_TOUCHE): 
        print("Vous avez déja tiré ici, retirez :")
        return tir.DEJA_TIRE
    elif(plateau[ligne][colonne] == case.VIDE):
        plateau[ligne][colonne] = case.VIDE_TOUCHE
        print("\n❌ Manqué, pas de bateau ici")
        return tir.MANQUE
    else : #cas où un bateau est touché
        print("\n💥 TOUCHE 💥")
        plateau[ligne][colonne] = case.BATEAU_TOUCHE
        #print(dic_bateaux)
        (type_bat, numero_bateau, numero_case) = trouver_case_bateau(dic_bateaux, ligne, colonne)
        bateau = dic_bateaux[type_bat]['Cases'][numero_bateau]
        #print("bateau", bateau)
        del bateau[numero_case]
        

        if(bateau_coulé(bateau)):
            print("\n☠️  BATEAU COULE ☠️")
            dic_bateaux[type_bat]["Nombre_sur_plateau"] -= 1
            del dic_bateaux[type_bat]['Cases'][numero_bateau]
            return tir.COULE
        return tir.TOUCHE

def placer_bateau_brut(joueur,nom_bateau,ligne_debut,colonne_debut,ligne_fin,colonne_fin):
    bateau = joueur['bateaux'][nom_bateau]
    if (ligne_debut>len(joueur['plateau'])-1) or (ligne_debut<0) or (ligne_fin>len(joueur['plateau'])-1) or (ligne_fin<0):
        return erreur_placer_bateau.CASE_EN_DEHORS_PLATEAU
    if (ligne_debut!=ligne_fin) and (colonne_debut!= colonne_fin):
        return erreur_placer_bateau.BATEAU_DIAGONAL
    if (abs(ligne_debut-ligne_fin+colonne_debut-colonne_fin)+1)!= bateau['Taille']:
        return erreur_placer_bateau.MAUVAISE_TAILLE_BATEAU
    for ligne in range(ligne_debut,ligne_fin+1):
        if (colonne_debut>len(joueur['plateau'][ligne])-1) or (colonne_debut<0) or (colonne_fin>len(joueur['plateau'][ligne])-1) or (colonne_fin<0):
          return erreur_placer_bateau.BATEAU_EN_DEHORS_PLATEAU
        for colonne in range(colonne_debut,colonne_fin+1):
            if joueur['plateau'][ligne][colonne]==case.BATEAU:
                return erreur_placer_bateau.BATEAU_SUR_AUTRE_BATEAU
    for ligne in range(ligne_debut,ligne_fin+1):
        for colonne in range(colonne_debut,colonne_fin+1):
            joueur['plateau'][ligne][colonne]=case.BATEAU
            #print("joueur", joueur)
            #print("bateau['Cases'][bateau['Nombre_sur_plateau']", bateau['Cases'][bateau['Nombre_sur_plateau']])
            #print("bateau['Nombre_sur_plateau']", bateau['Nombre_sur_plateau'])
            (bateau['Cases'][bateau['Nombre_sur_plateau']]).append([case.BATEAU, ligne, colonne])
    return erreur_placer_bateau.OK

def placer_bateaux(joueur):
    bateau = joueur['bateaux']
    while(
        bateau['PA']['Nombre'] != bateau['PA']['Nombre_sur_plateau']or
        bateau['CT']['Nombre'] != bateau['CT']['Nombre_sur_plateau'] or
        bateau['C']['Nombre'] != bateau['C']['Nombre_sur_plateau'] or
        bateau['P']['Nombre'] != bateau['P']['Nombre_sur_plateau']):
            afficher_plateau(joueur['plateau'])
            afficher_bateaux(joueur)
            choix=input("Choix du bateau : >")
            while (choix not in bateau) or (bateau[choix]['Nombre']-bateau[choix]['Nombre_sur_plateau']) <=0:
                print("nombre de",choix,"insuffisant")
                choix=input("Choix du bateau : >")

            case1_x=(input("Ligne première case : >"))
            while not(str(case1_x).isdigit() and len(str(case1_x)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                case1_x=(input("Ligne première case : >"))
            case1_y=(input("Colonne première case : >"))
            while not(str(case1_y).isdigit() and len(str(case1_y)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                case1_y=(input("Colonne première case : >"))
            case2_x=(input("Ligne dernière case : >"))
            while not(str(case2_x).isdigit() and len(str(case2_x)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                case2_x=(input("Ligne dernière case : >"))
            case2_y=(input("Colonne dernière case : >"))
            while not(str(case2_y).isdigit() and len(str(case2_y)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                case2_y=(input("Colonne dernière case : >"))

            if placer_bateau_brut(joueur,choix,int(case1_x),int(case1_y),int(case2_x),int(case2_y))==erreur_placer_bateau.OK:
                bateau[choix]['Nombre_sur_plateau']+=1
            elif placer_bateau_brut(joueur,choix,int(case1_x),int(case1_y),int(case2_x),int(case2_y))==erreur_placer_bateau.BATEAU_EN_DEHORS_PLATEAU:
                print("\nErreur!! Le bateau n'a pas pu être posé, vous tentez de le placer en dehors du plateau !")
            elif placer_bateau_brut(joueur,choix,int(case1_x),int(case1_y),int(case2_x),int(case2_y))==erreur_placer_bateau.BATEAU_DIAGONAL:
                print("\nErreur!! Le bateau n'a pas pu être posé, vous tentez de le placer en diagonal ! Il doit être sur une seule ligne ou une seule colonne !")
            elif placer_bateau_brut(joueur,choix,int(case1_x),int(case1_y),int(case2_x),int(case2_y))==erreur_placer_bateau.BATEAU_SUR_AUTRE_BATEAU:
                print("\nErreur!! Le bateau n'a pas pu être posé, certaines de ses cases sont déjà occupées par un bateau du plateau !")
            elif placer_bateau_brut(joueur,choix,int(case1_x),int(case1_y),int(case2_x),int(case2_y))==erreur_placer_bateau.MAUVAISE_TAILLE_BATEAU:
                print("\nErreur!! Le bateau n'a pas pu être posé, les cases choisies ne permettent pas de respecter la taille du bateau !")
def clear_terminal():
    # Windows
    if os.name == 'nt':
        _ = os.system('cls')
    # Unix/Linux/Mac
    else:
        _ = os.system('clear')
        
def afficher_plateau(plateau):
    print("\n    ", end="")
    for colonne in range(10):
        print(colonne,end=" ")
    print()
    for ligne in range(10):
        print(ligne,"[",end=" ")
        for emplacement in plateau[ligne]:
            if emplacement == case.VIDE:
                print("⬝",end=" ")
            if emplacement == case.BATEAU:
                print("B",end=" ")
            if emplacement == case.VIDE_TOUCHE:
                print("X",end=" ")
            if emplacement == case.BATEAU_TOUCHE:
                print("T",end=" ")

        print("]")
    print()

def affichage_masque(plateau):
    print("\n    ", end="")
    for colonne in range(10):
        print(colonne,end=" ")
    print()
    for ligne in range(10):
        print(ligne,"[",end=" ")
        for emplacement in plateau[ligne]:
            if emplacement == case.VIDE or emplacement == case.BATEAU:
                print("⬝",end=" ")
            if emplacement == case.VIDE_TOUCHE:
                print(" ",end=" ")
            if emplacement == case.BATEAU_TOUCHE:
                print("X",end=" ")

        print("]")
    print()

def afficher_bateaux(joueur):
    print("Voici les bateaux qu'il vous reste à placer :")
    for chaque_type_bateau in joueur['bateaux'].keys():
        type_bateau = joueur['bateaux'][chaque_type_bateau]
        if(type_bateau["Nombre"]>type_bateau["Nombre_sur_plateau"]):
            print("\t▸ ", chaque_type_bateau, " (taille:", type_bateau['Taille'], ", nombre:", type_bateau['Nombre']-type_bateau['Nombre_sur_plateau'], ")", sep="")
print()

def affichage_encadre(chaine):
    print(" ", sep="", end="")
    for i in range(len(chaine)+2):
        print("_", sep="", end="")
    print("\n|", chaine,"|")
    print(" ", sep="", end="")
    for i in range(len(chaine)+2):
        print("‾", sep="", end="")
    print()

def affichage_deb_partie_joueur(joueur):
    affichage_encadre("Tour : " + joueur['name'])
    print("Place tes bateaux")

def echange_joueur(A,B):
    return (B,A)

#fonction pour une seule partie
def partie(joueur1,joueur2):
    #bateaux=[]
    #nb_bateaux_j1 = dict(joueur1['bateaux']) #on fait une copie
    #nb_bateaux_j2 = dict(joueur2['bateaux'])
    init_partie()
    clear_terminal()
    affichage_deb_partie_joueur(joueur1)
    placer_bateaux(joueur1)
    clear_terminal()
    #print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    affichage_deb_partie_joueur(joueur2)
    placer_bateaux(joueur2)
    clear_terminal()
    #print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    joueur_action = joueur1
    joueur_actuel = joueur_action['name']
    joueur_attente = joueur2
    while(reste_bateaux_grille(joueur1, joueur2)):
        val_tir = tir.TOUCHE
        while(val_tir == tir.TOUCHE):
            val_tir = tour_joueur(joueur_action, joueur_attente)
        if(val_tir==tir.MANQUE):
            clear_terminal()
            (joueur_action, joueur_attente)=echange_joueur(joueur_action,joueur_attente)
        joueur_actuel = joueur_action['name']

    print("\n⚓ FIN DE PARTIE ⚓\n")
    print(joueur_action['name'], "a gagné !")
    joueur_action['score']+=1
    print("\nActuellement :\n\t",joueur1['name'], " a un score de", joueur1['score'],
          "\n\t",joueur2['name'], " a un score de", joueur2['score'])

def tour_joueur(joueur, adversaire):
    #clear_terminal()
    affichage_encadre("Tour : " + joueur['name'])
    val_tir = tir.DEJA_TIRE
    while(val_tir==tir.DEJA_TIRE):
        print("Voici le plateau de l'adversaire : ")
        affichage_masque(adversaire['plateau'])
        ligne = (input("Quelle ligne de la grille visez-vous : >"))
        while not(str(ligne).isdigit() and len(str(ligne)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                ligne = (input("Quelle ligne de la grille visez-vous : >"))
        colonne = (input("Quelle colonne de la grille visez-vous : >"))
        while not(str(colonne).isdigit() and len(str(colonne)) == 1):
                print("Erreur!! Entrez un chiffre entre 0 et 9 seulement!")
                colonne = (input("Quelle colonne de la grille visez-vous : >"))
        val_tir = tirer(adversaire,int(ligne),int(colonne))
    return val_tir

#fonction retournant 1 s'il reste des bateaux à couler pour les deux joueurs, retourne 0 sinon
def reste_bateaux_grille(joueur1, joueur2):
    #print("joueur1", joueur1)
    #print("joueur2", joueur2)
    nb_bateaux_j1 = 0
    nb_bateaux_j2 = 0
    for bateau in joueur1['bateaux'].values():
        nb_bateaux_j1 += bateau['Nombre_sur_plateau']
    for bateau in joueur2['bateaux'].values():
        nb_bateaux_j2 += bateau['Nombre_sur_plateau']
    if(nb_bateaux_j1>0 and nb_bateaux_j2>0):
        return 1
    return 0 # au moins 1 des 2 joueurs n'a plus de bateau

def jouer(joueur1, joueur2):
    clear_terminal()
    print("💥💥💥💥💥💥💥💥💥💥💥")
    print("⚓  BATAILLE NAVALE ⚓")
    print("💥💥💥💥💥💥💥💥💥💥💥")
    print("\n🛥  Bienvenue dans le jeu de bataille navale\n🛥  En cas de besion, utilise le manuel utilisateur (README.MD)\n🛥  Bonne chance !!!\n")
    init_joueurs()
    rejouer = 1
    while(rejouer):
        partie(joueur1,joueur2)
        rejouer = -1
        while(rejouer!=1 and rejouer!=0):
            print("\nVoulez-vous rejouer ? Entrez :")
            print("\t1 pour rejouer")
            print("\t0 pour arreter")
            rejouer = int(input(">"))
    print("Fin du JEU")
        

#jouer(joueur1, joueur2)


