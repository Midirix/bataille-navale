import pygame
import pygame_menu
import pygame_menu.menu
import pygame_menu.themes
from menu import *
from Sprite import *
from enum import Enum
from fonction import *

class etat_programme(Enum):
    MENU_PRINCIPALE = 0
    EN_JEU = 1
    EN_PAUSE = 2
    
class etat_jeu(Enum):
    SETUP_A = 0
    SETUP_B = 1
    PLAYING = 2

vec = pygame.math.Vector2

class Jeu():
    def __init__(self):
        pygame.init()
        pygame.font.init()
        pygame.display.set_caption("Bataille navale")
        logo = pygame.image.load("logo.png")
        pygame.display.set_icon(logo)
        self.running = True
        self.pause_released = True
        
        self.prog_state = etat_programme.MENU_PRINCIPALE
        self.game_state = etat_jeu.SETUP_A
        #New window
        self.screen = pygame.display.set_mode((1280, 620), pygame.RESIZABLE)
        #Menu parametres
        self.menu_parameter = pygame_menu.Menu("Paramètres", 900, 600, theme=pygame_menu.themes.THEME_DARK)
        self.menu_parameter.add.toggle_switch("Plein écran", 0, self.methode_fullscreen)
        self.menu_parameter.center_content()
        #Menu principal
        self.menu_main = pygame_menu.Menu("La bataille navale", 900, 600, theme=pygame_menu.themes.THEME_BLUE)
        self.menu_main.add.button("Nouvelle partie", self.new_game_handler)
        self.menu_main.add.button("Reprendre partie", resume_game_handler)
        self.menu_main.add.button("Paramètres", self.menu_parameter)
        self.menu_main.add.button("Quitter", pygame_menu.events.EXIT)
        self.menu_main.set_onclose(pygame_menu.events.CLOSE)
        self.menu_main.center_content()
        #Menu pause
        self.menu_pause = pygame_menu.Menu("Pause", 900, 600, theme=pygame_menu.themes.THEME_DARK)
        self.menu_pause.add.button("Reprendre", self.resume_handler)
        self.menu_pause.add.button("Paramètres", self.menu_parameter)
        self.menu_pause.add.button("Quitter", pygame_menu.events.EXIT)
        self.menu_pause.center_content()
        self.menu_pause.disable()
        #Load background
        self.background0 = pygame.image.load("background.png")
        self.background0.convert_alpha()
        self.background = self.background0
        self.game_background0 = pygame.image.load("game_background.png")
        self.game_background0.convert_alpha()
        self.game_background = self.game_background0
        #Load pins
        self.pinR0 = pygame.image.load("pin_red1.png")
        self.pinW0 = pygame.image.load("pin_white1.png")
        self.pinR0.convert()
        self.pinW0.convert()
        self.pinR = self.pinR0
        self.pinW = self.pinW0
        #Plateau parameters
        self.plateauA_origin = vec(0,0)
        self.plateauB_origin = vec(0,0)
        self.window_scale = 1.0
        self.plateau_scale = 0.85
        self.game_scale = 3.55
        self.font_scale = 60
        self.plateau_center_offset = 100
        self.plateau_A_boats = self.init_plateau_boat()
        self.plateau_A_init_done = False
        self.plateau_B_boats = self.init_plateau_boat()
        self.plateau_B_init_done = False
        #Redraw background
        self.render_rect = pygame.Rect(0,0,0,0)
        self.render_rect_old = pygame.Rect(0,0,0,0)      
        
        #Boat
        self.boats_A = {}
        self.boats_A.update({"boat1": Boat(clip(pygame.image.load("boats.png"), 200, 0, 200, 400), 0, 2, vec(0,10.5), 90) })
        self.boats_A.update({"boat2": Boat(clip(pygame.image.load("boats.png"), 0, 0, 200, 600), 0, 3, vec(2,10.5), 90) })
        self.boats_A.update({"boat3": Boat(clip(pygame.image.load("boats.png"), 400, 200, 600, 200), 90, 3, vec(0,11.5), 90) })
        self.boats_A.update({"boat4": Boat(clip(pygame.image.load("boats.png"), 400, 000, 800, 200), 90, 4, vec(5,10.5), 90) })
        self.boats_A.update({"boat5": Boat(clip(pygame.image.load("boats.png"), 1000, 200, 200, 1000), 0, 5, vec(4,11.5), 90) })
        self.boats_B = {}
        self.boats_B.update({"boat1": Boat(clip(pygame.image.load("boats.png"), 600, 400, 200, 400), 0, 2, vec(0,10.5), 90) })
        self.boats_B.update({"boat2": Boat(clip(pygame.image.load("boats.png"), 0, 600, 600, 200), 90, 3, vec(2,10.5), 90) })
        self.boats_B.update({"boat3": Boat(clip(pygame.image.load("boats.png"), 400, 200, 600, 200), 90, 3, vec(0,11.5), 90) })
        self.boats_B.update({"boat4": Boat(clip(pygame.image.load("boats.png"), 0, 800, 800, 200), 90, 4, vec(5,10.5), 90) })
        self.boats_B.update({"boat5": Boat(clip(pygame.image.load("boats.png"), 0, 1000, 1000, 200), 90, 5, vec(4,11.5), 90) })
            
        
        init_joueurs()
        

    def jeu_loop(self):
        events = pygame.event.get()
        for event in events:
            #Handle the quit event
            if event.type == pygame.QUIT:
                self.running = False
            #Handle window resizing
            if event.type == pygame.VIDEORESIZE:
                on_resize(self)
            #ESC key behavior for pause menu
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    print("ESC")
                    if self.prog_state != etat_programme.EN_PAUSE and self.pause_released == True:
                        print("PAUSE")
                        self.prog_state = etat_programme.EN_PAUSE
                        self.pause_released = False
                        self.menu_pause.enable()
                    if self.prog_state == etat_programme.EN_PAUSE and self.pause_released == True:
                        print("UNPAUSE")
                        self.prog_state = etat_programme.EN_JEU
                        self.pause_released = False
                        self.resume_handler()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    self.pause_released = True
            #ENTER key behavior
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_o:
                    print("Gamestate : ",self.game_state)
                    if self.game_state == etat_jeu.SETUP_A:
                        print(self.is_boats_initialized(0))
                        if self.is_boats_initialized(0):
                            self.plateau_A_init_done = True
                            self.game_state = etat_jeu.SETUP_B
                    
            #Game loop
            if self.prog_state == etat_programme.EN_JEU:
                #Clear screen and draw background
                if True:
                    self.screen.fill((0,0,0))
                    self.draw_background()
                self.print_text()   # print tout les textes sur l'ecran

                # Boat loop
                for boat in  self.boats_A.values():
                    if event.type == pygame.MOUSEBUTTONDOWN and not self.plateau_A_init_done:
                        if boat.rect.collidepoint(event.pos):
                            self.remove_boat_from_plateau(boat)
                            boat.moving = True
                    if boat.moving:
                        if event.type == pygame.MOUSEBUTTONUP:
                            boat.snap_to_grid(self)
                            boat.moving = False
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_r:
                                boat.rotate()
                        if event.type == pygame.MOUSEMOTION:
                            boat.rect.move_ip(event.rel)
                            boat.position += event.rel
                            boat.update_plateau_position(self)                    
                    boat.blit_sprite(self.screen)
                ######
            #Main menu loop
            if self.prog_state == etat_programme.MENU_PRINCIPALE:
                self.screen.fill((25, 0, 50))
        
        #Handle whether to render menus or not    
        #Statements must be separated
        if self.menu_main.is_enabled():
            self.menu_main.update(events)
        if self.menu_main.is_enabled():    
            self.menu_main.draw(self.screen)
        if self.menu_pause.is_enabled():
            self.menu_pause.update(events)
        if self.menu_pause.is_enabled():    
            self.menu_pause.draw(self.screen)
        pygame.display.flip()
       

    def draw_background(self):
        #Get size of the screen
        screen_H = self.screen.get_height()
        screen_L = self.screen.get_width()
        #Origins of board A and B
        self.plateauA_origin = vec(screen_L/2-self.background.get_width()-self.plateau_center_offset*self.window_scale, 0)
        self.plateauB_origin = vec(screen_L/2+self.plateau_center_offset*self.window_scale, 0)
        #Depending on the aspect ratio of the screen:                
        if screen_L/screen_H > ((self.background0.get_width()+self.plateau_center_offset)*2)/self.background0.get_height():
            self.window_scale = screen_H/self.background0.get_height()
            self.background = pygame.transform.rotozoom(self.background0, 0, self.window_scale*self.plateau_scale)
            self.game_background = pygame.transform.rotozoom(self.game_background0, 0, self.window_scale*self.game_scale)
            self.screen.blit(self.game_background, (0,-50))
            self.screen.blit(self.background, self.plateauA_origin)
            self.screen.blit(self.background, self.plateauB_origin)
        else:
            self.window_scale = (screen_L/2.0)/(self.background0.get_width()+self.plateau_center_offset)
            self.background = pygame.transform.rotozoom(self.background0, 0, self.window_scale*self.plateau_scale)
            self.game_background = pygame.transform.rotozoom(self.game_background0, 0, self.window_scale*self.game_scale)
            self.screen.blit(self.game_background, (0,-50))
            self.screen.blit(self.background, self.plateauA_origin)
            self.screen.blit(self.background, self.plateauB_origin)    

    def draw_pin(self, plateau, type, position):
        #Use the selected image
        if type == 0:
            pin = self.pinW0
        else:
            pin = self.pinR0
        #Scale image            
        pin = pygame.transform.rotozoom(pin, 0, self.window_scale*self.plateau_scale*0.375)
        #Get center of image
        center = vec(pin.get_width()/2, pin.get_height()/2)
        return self.screen.blit(pin, self.plateau_to_absolute(plateau, position, center))    #Draw
        
    def methode_fullscreen(self, state):
        if state != pygame.display.is_fullscreen():
            pygame.display.toggle_fullscreen()
        on_resize(self)

    def new_game_handler(self):
        self.prog_state = etat_programme.EN_JEU
        self.menu_main.close()
        print("Commencer une nouvelle partie")
        self.screen.fill((0,0,0))
        self.draw_background()
        on_resize(self)
        # Place each boat
        for boat in self.boats_A.values():
            boat.update_scale(self)
            boat.set_rotation(boat.rotation_base)
            boat.set_position_game(boat.position_plateau_base, 0, self)
            boat.blit_sprite(self.screen)
            print(boat.position)
            print(boat.rect)
        pygame.display.flip()

    def resume_handler(self):
        self.prog_state = etat_programme.EN_JEU
        self.menu_pause.close()
        self.menu_pause.disable()
        self.screen.fill((0, 0, 0))
        print("Resume handled")          

    #Draw each plateau coordinates
    def draw_coord(self, plateau, position, text, font, text_col):
        img = font.render(text, True, text_col)
        #Get center of text
        center = vec(img.get_width()/2, img.get_height()/2)
        self.screen.blit(img, self.plateau_to_absolute(plateau, position, center))
        
    def draw_boats(self):
        for boat in self.boats_A.values():
            boat.set_position_game(vec(0,0), 0, self)
            boat.blit_sprite(self.screen)
    
    #Convert plateau coordinates to pixel coordinates
    def plateau_to_absolute(self, plateau, position, center):
        case_width = 91
        base = vec(case_width,case_width)*self.window_scale*self.plateau_scale
        coords = position*case_width*self.window_scale*self.plateau_scale
        #Get origin of plateau
        if plateau == 0:
            origin = self.plateauA_origin
        else:
            origin = self.plateauB_origin
        origin = origin - center  #Offset to center of image
        origin = origin + base    #Offset to first case of plateau
        return origin+coords
    
    #Convert pixel coordinates to plateau coordinates
    def absolute_to_plateau(self, plateau, position, center):
        case_width = 91
        if plateau == 0:
            origin = self.plateauA_origin
        else:
            origin = self.plateauB_origin
        origin = origin - center
        base = vec(case_width,case_width)*self.window_scale*self.plateau_scale
        origin = origin + base
        position = position - origin
        return position/(case_width*self.window_scale*self.plateau_scale)        
    
    #Draw the text on screen
    def print_text(self):
        #Initialize font
        text_font = pygame.font.Font(None, int(round(self.font_scale*self.window_scale*self.plateau_scale)))
        text_color = (38,18,12)
        #Draw letters for both boards
        for i in range(0,10):
            self.draw_coord(0, vec(i, -0.65), chr(65+i), text_font, text_color)
            self.draw_coord(1, vec(i, -0.65), chr(65+i), text_font, text_color)
        #Draw numbers for both boards            
        for j in range(0,10):
            self.draw_coord(0, vec(-0.65, j), str(j+1), text_font, text_color)
            self.draw_coord(1, vec(-0.65, j), str(j+1), text_font, text_color)

        self.draw_coord(0, vec(5,10.2),"mon camp", text_font, (0,100,0))
        self.draw_coord(1, vec(5,10.2),"camp ennemi", text_font, (100,0,0))
        self.draw_coord(0, vec(10,10.2),"Au tour de " + joueur_actuel + " !", text_font, (0,0,255))

    #Create a 10x10 grid        
    def init_plateau_boat(self):
        return [[0 for x in range(10)] for y in range(10)]
    
    #Checks if the boat position is valid 
    def is_boat_position_valid(self, boat, position):
        if boat.rotation%180 == 0:
            for i in range(boat.size):
                if self.plateau_A_boats[int(position.y)+i][int(position.x)] == 1:
                    return False
        else:
            for i in range(boat.size):
                if self.plateau_A_boats[int(position.y)][int(position.x)+i] == 1:
                    return False
        return True
    
    #Update the plateau grid with boat position
    def place_boat_on_plateau(self, boat):   
        if boat.rotation%180 == 0:
            for i in range(boat.size):
                self.plateau_A_boats[int(boat.position_plateau.y)+i][int(boat.position_plateau.x)] = 1
        else:
            for i in range(boat.size):
                self.plateau_A_boats[int(boat.position_plateau.y)][int(boat.position_plateau.x)+i] = 1
    
    #Remove the boat from the plateau grid
    def remove_boat_from_plateau(self, boat):
        if not boat.initialized:
            return
        if boat.rotation%180 == 0:
            for i in range(boat.size):
                self.plateau_A_boats[int(boat.position_plateau.y)+i][int(boat.position_plateau.x)] = 0
        else:
            for i in range(boat.size):
                self.plateau_A_boats[int(boat.position_plateau.y)][int(boat.position_plateau.x)+i] = 0
    
    #Return true is all boats set
    def is_boats_initialized(self, player):
        for boat in self.boats_A.values():
            if not boat.initialized:
                return False
        return True
        