import pygame

vec = pygame.math.Vector2

def resume_game_handler():
    print("Reprendre la partie")       

def on_resize(jeu):
    window_size = jeu.screen.get_size()
    new_w = 0.80 * window_size[0]
    new_h = 0.80 * window_size[1]
    jeu.menu_main.resize(new_w, new_h)
    jeu.menu_parameter.resize(new_w, new_h)
    print(f'New menu size: {jeu.menu_main.get_size()}')
    jeu.screen.fill((0,0,0))
    jeu.draw_background()
    jeu.redraw_bkg = 3
    for boat in jeu.boats_A.values():
        boat.update_scale(jeu)
        boat.position = jeu.plateau_to_absolute(0, boat.position_plateau, vec(boat.width/2, boat.width/2))
    for boat in jeu.boats_B.values():
        boat.update_scale(jeu)
        boat.position = jeu.plateau_to_absolute(0, boat.position_plateau, vec(boat.width/2, boat.width/2))
    
    pygame.display.flip()
    
    
def clip(surface, x, y, x_size, y_size): #Get a part of the image
    handle_surface = surface.copy() #Sprite that will get process later
    clipRect = pygame.Rect(x,y,x_size,y_size) #Part of the image
    handle_surface.set_clip(clipRect) #Clip or you can call cropped
    image = surface.subsurface(handle_surface.get_clip()) #Get subsurface
    return image.copy() #Return
    
